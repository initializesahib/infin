/*
 * Copyright 2018 Sahibdeep Nann.
 *
 * This file is part of Infin.
 *
 * Infin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infin.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.initializesahib.infin;

import javax.security.auth.login.LoginException;

import io.github.cdimascio.dotenv.Dotenv;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.command.CommandClientBuilder;

public class Infin {
    public static JDA connect(String[] args) {
        Dotenv envVars = Dotenv.configure().directory(".").load();
        EventWaiter waiter = new EventWaiter();
        CommandClientBuilder client = new CommandClientBuilder();
        client.useDefaultGame();
        client.setOwnerId(envVars.get("OWNER"));
        client.setEmojis("\u2705", "\u26A0", "\u26D4");
        client.setPrefix("infin ");
        try {
            JDA bot = new JDABuilder(AccountType.BOT)
                    .setToken(envVars.get("TOKEN"))
                    .setStatus(OnlineStatus.ONLINE)
                    .setGame(Game.playing("Initializing v1.0.0"))
                    .addEventListener(waiter)
                    .addEventListener(client.build())
                    .build();
            bot.awaitReady();
            return bot;
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void main(String[] args) {
        Infin.connect(args);
    }
}
