import org.junit.Test;
import static org.junit.Assert.fail;
import com.initializesahib.infin.Infin;
import net.dv8tion.jda.core.JDA;

public class InfinTest {
    @Test
    public void botShouldConnectSuccessfullyToDiscord() {
        JDA connection = Infin.connect(new String[]{});
        if (connection == null) {
            fail("Connecting encountered a LoginException or InterruptedException");
        } else {
            connection.shutdownNow();
        }
    }
}
